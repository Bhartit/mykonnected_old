@extends('layouts.app')
@section('content')
    <div class="content view-content scroll">
        @if(isset($edit))
        	@include("dashboard.orgdefinition.edit")
        @else
		    @include("dashboard.orgdefinition.create")
        @endif

    </div>

@endsection
@section('left2')
    @include('foldersection.left2')
@endsection
