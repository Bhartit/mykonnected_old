@extends('layouts.app')
@section('content')
	
	<div class="content view-content scroll">
		<div class="view-overflow-container">
				@include('dashboard.horizontal')
				<div class="layout-row row-spacing"></div>
				@include("common._filter-menu")
				<div class="layout-row">
					@include("dashboard.goal.filter")	
				</div>
		
				<div id="ajaxContent">
					@include("dashboard.goal.datatable")
				</div>
		</div>
	</div>
@include('dashboard.goal.contextmenu')		
@endsection

@section('left2')
    @include('foldersection.left2')
@endsection
@section('script')
	<link rel="stylesheet/less" href="{{asset('resource/ui/components/pie-graph/pie.less')}}" type="text/css" />
	<script type="text/javascript" src="{{asset('resource/ui/components/pie-graph/less.min.js')}}"></script>
@endsection