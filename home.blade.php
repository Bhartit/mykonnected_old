@extends('layouts.app')
@section('content')
	<script>var menu = []</script>
	<div class="content view-content scroll">
		<div class="view-overflow-container">
				@include('dashboard.horizontal')
				<div class="layout-row row-spacing"></div>
				@include("dashboard.home._filter-menu")
				<div class="layout-row">
					@include("dashboard.home.filter")
				</div>

				<div id="ajaxContent">
					@include("dashboard.home.datatable")
				</div>
		</div>
	</div>
@endsection

@section('left2')
    @include('foldersection.left2')
@endsection
@section('script')
	<link rel="stylesheet/less" href="{{asset('resource/ui/components/pie-graph/pie.less')}}" type="text/css" />
	<script type="text/javascript" src="{{asset('resource/ui/components/pie-graph/less.min.js')}}"></script>
@endsection